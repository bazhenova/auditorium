<?php

namespace App\Controllers;

use CodeIgniter\RESTful\ResourceController;
use App\Services\OAuth;
use OAuth2\Request;

class ClassroomApi extends ResourceController
{
    protected $modelName = 'App\Models\ClassroomModel';
    protected $format = 'json';
    protected $oauth;

    public function classroom() //Отображение всех записей
    {
        $oauth = new OAuth();
        if ($oauth->isLoggedIn()) {
            return $this->respond($this->model->getClassroom());
        }
        $oauth->server->getResponse()->send();
    }

}