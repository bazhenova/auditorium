<?php namespace App\Controllers;

use App\Models\ClassroomModel;
use App\Models\DepartmentModel;
use App\Models\EquipmentModel;
use App\Models\EquipmentNumValueModel;
use Aws\S3\S3Client;
use CodeIgniter\Model;
use Throwable;

class Classroom extends BaseController

{
    public function index() //Обображение всех записей
    {
        //Подготовка значения количества элементов выводимых на одной странице
        if (!is_null($this->request->getPost('per_page'))) //если кол-во на странице есть в запросе
        {
            //сохранение кол-ва страниц в переменной сессии
            session()->setFlashdata('per_page', $this->request->getPost('per_page'));
            $per_page = $this->request->getPost('per_page');
        } else {
            $per_page = session()->getFlashdata('per_page');
            session()->setFlashdata('per_page', $per_page); //пересохранение в сессии
            if (is_null($per_page)) $per_page = '5'; //кол-во на странице по умолчанию
        }
        $data['per_page'] = $per_page;
        helper(['form', 'url']);

        $model = new ClassroomModel();
        $data ['classroom'] = $model->getClassroomByPage()->paginate($per_page, 'group1');
        $data['pager'] = $model->pager;
        echo view('classroom/view_all', $this->withIon($data));

    }

    public function view($id = null) //отображение одной записи
    {
        $model = new ClassroomModel();
        $model_extra = new EquipmentNumValueModel();
        $data ['classroom'] = $model->getClassroomWithDepartment($id);
        $data ['equipment'] = $model_extra->getValuesWithEquipment($id);
        echo view('classroom/view', $this->withIon($data));
    }

    public function create()
    {
        if (!$this->ionAuth->loggedIn()) {
            return redirect()->to('/auth/login');
        }
        if (!$this->ionAuth->isAdmin()) {
            session()->setFlashdata('message', lang('Auditorium.admin_permission_needed'));
            return redirect()->to('/auth/login');
        }
        helper(['form']);
        $model_d = new DepartmentModel();
        $model_e = new EquipmentModel();
        $data ['department'] = $model_d->getDepartment();
        $data ['equipment'] = $model_e->getEquipment();

        $data ['validation'] = \Config\Services::validation();
        echo view('classroom/create', $this->withIon($data));
    }

    public function store()
    {
        helper(['form', 'url']);

        if ($this->request->getMethod() === 'post' && $this->validate([
                'building' => 'required|exact_length[1]',
                'number' => 'required|regex_match[/^\d{3,3}((\/|-){1}(\d{1}|[A-ZА-ЯЁa-zа-яё]{1}))?$/u]', //regex_match[/^\d{3}((\/|-)(\d|[A-ZА-ЯЁ]|[a-zа-яё])?)?$/u] ^\d{3}( [\/-][\dA-ZА-ЯЁa-zа-яё] )?$
                'type' => 'required|exact_length[2]',
                'department_id' => 'required|is_natural_no_zero',
                'area' => 'required|regex_match[/^\d{1,4}(\.{1}\d{1})?$/u]',
                'windows' => 'required|is_natural',
                'capacity' => 'required|is_natural',
                'smartboards' => 'required|is_natural',
                'stat_screens' => 'required|is_natural',
                'stat_projectors' => 'required|is_natural',
                'chalkboards' => 'required|is_natural',
                'computers' => 'required|is_natural',
                'move_projectors' => 'required|is_natural',
                'sinks' => 'required|is_natural',
                'note' => 'permit_empty',
                'picture' => 'is_image[picture]|max_size[picture,1024]',
            ])) {
            $insert = null;
            //получение загруженного файла из HTTP-запроса
            $file = $this->request->getFile('picture');
            if ($file->getSize() != 0) {
                //подключение хранилища
                $s3 = new S3Client([
                    'version' => 'latest',
                    'region' => 'us-east-1', //us-east-1
                    'endpoint' => getenv('S3_ENDPOINT'),
                    'use_path_style_endpoint' => true,
                    'credentials' => [
                        'key' => getenv('S3_KEY'), //чтение настроек окружения из файла .env
                        'secret' => getenv('S3_SECRET'), //чтение настроек окружения из файла .env
                    ],
                ]);
                //получение расширения имени загруженного файла
                $ext = explode('.', $file->getName());
                $ext = $ext[count($ext) - 1];
                //загрузка файла в хранилище
                $insert = $s3->putObject([
                    'Bucket' => getenv('S3_BUCKET'), //чтение настроек окружения из файла .env
                    //генерация случайного имени файла
                    'Key' => getenv('S3_KEY') . '/file' . rand(100000, 999999) . '.' . $ext,
                    'Body' => fopen($file->getRealPath(), 'r+')
                ]);
            }

            $model = new ClassroomModel();
            $data = [
                'building' => $this->request->getPost('building'),
                'number' => $this->request->getPost('number'),
                'type' => $this->request->getPost('type'),
                'department_id' => $this->request->getPost('department_id'),
                'area' => $this->request->getPost('area'),
                'windows' => $this->request->getPost('windows'),
                'capacity' => $this->request->getPost('capacity'),
                'smartboards' => $this->request->getPost('smartboards'),
                'stat_screens' => $this->request->getPost('stat_screens'),
                'stat_projectors' => $this->request->getPost('stat_projectors'),
                'chalkboards' => $this->request->getPost('chalkboards'),
                'computers' => $this->request->getPost('computers'),
                'move_projectors' => $this->request->getPost('move_projectors'),
                'sinks' => $this->request->getPost('sinks'),
                'note' => $this->request->getPost('note'),
            ];
            if (!is_null($insert))
                $data['picture_url'] = $insert['ObjectURL'];

            $id = $model->getId($data['building'], $data['number']);
            if (empty($id))
                $model ->save($data);
            else
            {
                session()->setFlashdata('message', lang('Auditorium.already_existed'));
                return redirect()->to('/classroom/create')->withInput();
            }

            $model_e = new EquipmentModel();
            $model_v = new EquipmentNumValueModel();

            $room_id = $model->getLastId();
            $equipment_id = $model_e->getEquipmentId();

            foreach ($equipment_id as $item => $value) {
                if (is_numeric($this->request->getPost($value['id']))) {
                    $data_v = [
                        'id_eqp' => $value['id'],
                        'id_room' => $room_id[0]['id'],
                        'value' => $this->request->getPost($value['id']),
                    ];
                    $model_v->save($data_v);
                }
            }

            session()->setFlashdata('message', lang('Auditorium.classroom_create_success'));
            return redirect()->to('/classroom');
        } else {
            return redirect()->to('/classroom/create')->withInput();
        }
    }

    public function edit($id)
    {
        if (!$this->ionAuth->loggedIn()) {
            return redirect()->to('/auth/login');
        }
        if (!$this->ionAuth->isAdmin()) {
            session()->setFlashdata('message', lang('Auditorium.admin_permission_needed'));
            return redirect()->to('/auth/login');
        }
        $model = new ClassroomModel();
        $model_d = new DepartmentModel();
        $model_e = new EquipmentModel();
        $model_v = new EquipmentNumValueModel();

        helper(['form']);
        $data ['classroom'] = $model->getClassroom($id);
        $data ['department'] = $model_d->getDepartment();
        $data ['equipment'] = $model_e->getEquipmentWithoutRoomValues($id);
        $data ['values'] = $model_v->getValuesWithEquipment($id);

        $data ['validation'] = \Config\Services::validation();
        echo view('classroom/edit', $this->withIon($data));
    }

    public function update()
    {
        helper(['form', 'url']);
        echo '/classroom/edit/' . $this->request->getPost('id');
        if ($this->request->getMethod() === 'post' && $this->validate([
                'id' => 'required',
                /*'building' => 'required|exact_length[1]',
                'number'  => 'required|min_length[3]|max_length[5]|regex_match[/^\d{3}((\/|-)?(\d|[A-ZА-ЯЁ]|[a-zа-яё])?)?$/u]',*/
                'type' => 'required|exact_length[2]',
                'department_id' => 'required|is_natural_no_zero',
                'area' => 'required|regex_match[/^\d{1,4}(\.{1}\d{1})?$/u]',
                'windows' => 'required|is_natural',
                'capacity' => 'required|is_natural',
                'smartboards' => 'required|is_natural',
                'stat_screens' => 'required|is_natural',
                'stat_projectors' => 'required|is_natural',
                'chalkboards' => 'required|is_natural',
                'computers' => 'required|is_natural',
                'move_projectors' => 'required|is_natural',
                'sinks' => 'required|is_natural',
                'note' => 'permit_empty',
                'picture' => 'is_image[picture]|max_size[picture,1024]',
            ])) {
            $insert = null;
            //получение загруженного файла из HTTP-запроса
            $file = $this->request->getFile('picture');
            if ($file->getSize() != 0) {
                //подключение хранилища
                $s3 = new S3Client([
                    'version' => 'latest',
                    'region' => 'us-east-1',
                    'endpoint' => getenv('S3_ENDPOINT'),
                    'use_path_style_endpoint' => true,
                    'credentials' => [
                        'key' => getenv('S3_KEY'), //чтение настроек окружения из файла .env
                        'secret' => getenv('S3_SECRET'), //чтение настроек окружения из файла .env
                    ],
                ]);
                //получение расширения имени загруженного файла
                $ext = explode('.', $file->getName());
                $ext = $ext[count($ext) - 1];
                //загрузка файла в хранилище
                $insert = $s3->putObject([
                    'Bucket' => getenv('S3_BUCKET'), //чтение настроек окружения из файла .env
                    //генерация случайного имени файла
                    'Key' => getenv('S3_KEY') . '/file' . rand(100000, 999999) . '.' . $ext,
                    'Body' => fopen($file->getRealPath(), 'r+')
                ]);
            }

            $model = new ClassroomModel();
            $data = [
                'id' => $this->request->getPost('id'),
                /*'building' => $this->request->getPost('building'),
                'number' => $this->request->getPost('number'),*/
                'type' => $this->request->getPost('type'),
                'department_id' => $this->request->getPost('department_id'),
                'area' => $this->request->getPost('area'),
                'windows' => $this->request->getPost('windows'),
                'capacity' => $this->request->getPost('capacity'),
                'smartboards' => $this->request->getPost('smartboards'),
                'stat_screens' => $this->request->getPost('stat_screens'),
                'stat_projectors' => $this->request->getPost('stat_projectors'),
                'chalkboards' => $this->request->getPost('chalkboards'),
                'computers' => $this->request->getPost('computers'),
                'move_projectors' => $this->request->getPost('move_projectors'),
                'sinks' => $this->request->getPost('sinks'),
                'note' => $this->request->getPost('note'),
            ];
            if (!is_null($insert))
                $data['picture_url'] = $insert['ObjectURL'];
            $model->save($data);

            $model_e = new EquipmentModel();
            $model_v = new EquipmentNumValueModel();
            $equipment_id = $model_e->getEquipmentId();
            $room = $model_v->getValuesByRoom($this->request->getPost('id'));

            foreach ($equipment_id as $item => $value) {
                if (is_numeric($this->request->getPost($value['id']))) {
                    $data_v = [
                        'id_eqp' => $value['id'],
                        'id_room' => $this->request->getPost('id'),
                        'value' => $this->request->getPost($value['id']),
                    ];
                    $updateFlag = false;
                    foreach ($room as $key => $room_value) {
                        if ($room_value['id_eqp'] == $data_v['id_eqp']) {
                            $model_v->update($room_value['id_v'], $data_v);
                            $updateFlag = true;
                            break;
                        }
                    }
                    if (!$updateFlag)
                        $model_v->save($data_v);
                }
            }


            session()->setFlashdata('message', lang('Auditorium.classroom_update_success'));

            return redirect()->to('/classroom');
        } else {
            return redirect()->to('/classroom/edit/' . $this->request->getPost('id'))->withInput();
        }
    }

    public function delete($id)
    {
        if (!$this->ionAuth->loggedIn()) {
            return redirect()->to('/auth/login');
        }
        if (!$this->ionAuth->isAdmin()) {
            session()->setFlashdata('message', lang('Auditorium.admin_permission_needed'));
            return redirect()->to('/auth/login');
        }

        $model_v = new EquipmentNumValueModel();
        $room = $model_v->getValuesByRoom($id);
        foreach ($room as $key => $room_value)
            $model_v->delete($room_value['id_v']);

        $model = new ClassroomModel();
        $model->delete($id);

        session()->setFlashdata('message', lang('Auditorium.classroom_delete_success'));
        return redirect()->to('/classroom');
    }

    public function search()
    {
        //Обработка запроса на поиск
        if (!is_null($this->request->getPost('building')) &&
            !is_null($this->request->getPost('type')) &&
            !is_null($this->request->getPost('department_id')) &&
            !is_null($this->request->getPost('area')) &&
            !is_null($this->request->getPost('windows')) &&
            !is_null($this->request->getPost('capacity')) &&
            !is_null($this->request->getPost('smartboards')) &&
            !is_null($this->request->getPost('stat_screens')) &&
            !is_null($this->request->getPost('stat_projectors')) &&
            !is_null($this->request->getPost('chalkboards')) &&
            !is_null($this->request->getPost('computers')) &&
            !is_null($this->request->getPost('move_projectors')) &&
            !is_null($this->request->getPost('sinks'))
        ) {
            session()->setFlashdata([
                'building' => $this->request->getPost('building'),
                'type' => $this->request->getPost('type'),
                'department_id' => $this->request->getPost('department_id'),
                'area' => $this->request->getPost('area'),
                'windows' => $this->request->getPost('windows'),
                'capacity' => $this->request->getPost('capacity'),
                'smartboards' => $this->request->getPost('smartboards'),
                'stat_screens' => $this->request->getPost('stat_screens'),
                'stat_projectors' => $this->request->getPost('stat_projectors'),
                'chalkboards' => $this->request->getPost('chalkboards'),
                'computers' => $this->request->getPost('computers'),
                'move_projectors' => $this->request->getPost('move_projectors'),
                'sinks' => $this->request->getPost('sinks'),
            ]);

            $searchBuilding = $this->request->getPost('building');
            $searchType = $this->request->getPost('type');
            $searchDepartment_id = $this->request->getPost('department_id');
            $searchArea = $this->request->getPost('area');
            $searchWindows = $this->request->getPost('windows');
            $searchCapacity = $this->request->getPost('capacity');
            $searchSmartboards = $this->request->getPost('smartboards');
            $searchStat_screens = $this->request->getPost('stat_screens');
            $searchStat_projectors = $this->request->getPost('stat_projectors');
            $searchChalkboards = $this->request->getPost('chalkboards');
            $searchComputers = $this->request->getPost('computers');
            $searchMove_projectors = $this->request->getPost('move_projectors');
            $searchSinks = $this->request->getPost('sinks');
        } else {
            $searchBuilding = session()->getFlashdata('building');
            $searchType = session()->getFlashdata('type');
            $searchDepartment_id = session()->getFlashdata('department_id');
            $searchArea = session()->getFlashdata('area');
            $searchWindows = session()->getFlashdata('windows');
            $searchCapacity = session()->getFlashdata('capacity');
            $searchSmartboards = session()->getFlashdata('smartboards');
            $searchStat_screens = session()->getFlashdata('stat_screens');
            $searchStat_projectors = session()->getFlashdata('stat_projectors');
            $searchChalkboards = session()->getFlashdata('chalkboards');
            $searchComputers = session()->getFlashdata('computers');
            $searchMove_projectors = session()->getFlashdata('move_projectors');
            $searchSinks = session()->getFlashdata('sinks');

            session()->setFlashdata([
                'building' => $searchBuilding,
                'type' => $searchType,
                'department_id' => $searchDepartment_id,
                'area' => $searchArea,
                'windows' => $searchWindows,
                'capacity' => $searchCapacity,
                'smartboards' => $searchSmartboards,
                'stat_screens' => $searchStat_screens,
                'stat_projectors' => $searchStat_projectors,
                'chalkboards' => $searchChalkboards,
                'computers' => $searchComputers,
                'move_projectors' => $searchMove_projectors,
                'sinks' => $searchSinks,
            ]);

            if (is_null($searchBuilding)) $searchBuilding = '0';
            if (is_null($searchType)) $searchType = '0';
            if (is_null($searchDepartment_id)) $searchDepartment_id = '0';
            if (is_null($searchArea)) $searchArea = '0';
            if (is_null($searchWindows)) $searchWindows = '0';
            if (is_null($searchCapacity)) $searchCapacity = '0';
            if (is_null($searchSmartboards)) $searchSmartboards = '0';
            if (is_null($searchStat_screens)) $searchStat_screens = '0';
            if (is_null($searchStat_projectors)) $searchStat_projectors = '0';
            if (is_null($searchChalkboards)) $searchChalkboards = '0';
            if (is_null($searchComputers)) $searchComputers = '0';
            if (is_null($searchMove_projectors)) $searchMove_projectors = '0';
            if (is_null($searchSinks)) $searchSinks = '0';
        }

        $data['searchArea'] = $searchArea;
        $data['searchWindows'] = $searchWindows;
        $data['searchCapacity'] = $searchCapacity;
        $data['searchSmartboards'] = $searchSmartboards;
        $data['searchStat_screens'] = $searchStat_screens;
        $data['searchStat_projectors'] = $searchStat_projectors;
        $data['searchChalkboards'] = $searchChalkboards;
        $data['searchComputers'] = $searchComputers;
        $data['searchMove_projectors'] = $searchMove_projectors;
        $data['searchSinks'] = $searchSinks;

        helper(['form', 'url']);

        $model = new ClassroomModel();
        $model_d = new DepartmentModel();
        $model_e = new EquipmentModel();

        $equipment_id = $model_e->getEquipmentId();
        $searchEqp = array();
        foreach ($equipment_id as $item => $value) {
            if (is_numeric($this->request->getPost($value['id'])) && $this->request->getPost($value['id']) != 0) {
                $searchEqp [] = [
                    'id_eqp' => $value['id'],
                    'value' => $this->request->getPost($value['id']),
                ];
            }
        }

        $data ['classroom'] = $model->getClassroomBySearch($searchEqp, null, $searchBuilding, $searchType, $searchDepartment_id, $searchArea, $searchWindows,
            $searchCapacity, $searchSmartboards, $searchStat_screens, $searchStat_projectors,
            $searchChalkboards, $searchComputers, $searchMove_projectors, $searchSinks);//->findAll();
        $data ['department'] = $model_d->getDepartment();
        $data ['equipment'] = $model_e->getEquipment();

        echo view('classroom/search', $this->withIon($data));
    }

    public function insert()
    {
        if (!$this->ionAuth->loggedIn()) {
            return redirect()->to('/auth/login');
        }
        if (!$this->ionAuth->isAdmin()) {
            session()->setFlashdata('message', lang('Auditorium.admin_permission_needed'));
            return redirect()->to('/auth/login');
        }
        helper(['form']);
        $data ['validation'] = \Config\Services::validation();
        echo view('classroom/add_eqp', $this->withIon($data));

    }

    public function add()
    {
        helper(['form', 'url']);
        if ($this->request->getMethod() === 'post' && $this->validate([
                'name' => 'required|min_length[1]|max_length[255]|is_unique[extra_equipment.name]',
            ])) {
            $model = new EquipmentModel();
            $data = [
                'name' => $this->request->getPost('name'),
            ];
            $model->save($data);
            session()->setFlashdata('message', lang('Auditorium.field_create_success'));
            return redirect()->to('/classroom/insert');
        } else {
            return redirect()->to('/classroom/insert')->withInput();
        }
    }

    public function del()
    {
        helper(['form', 'url']);
        $model_e = new EquipmentModel();
        //Обработка запроса на поиск
        if (!is_null($this->request->getPost('equipment_id'))) {
            $equipment_id = $this->request->getPost('equipment_id');
            $model_e->delete($equipment_id);
            session()->setFlashdata('message', lang('Auditorium.field_delete_success'));
        }
        $data ['equipment'] = $model_e->getEquipment();
        echo view('classroom/del_eqp', $this->withIon($data));
    }

    public function import()
    {
        if (!$this->ionAuth->loggedIn()) {
            return redirect()->to('/auth/login');
        }
        if (!$this->ionAuth->isAdmin()) {
            session()->setFlashdata('message', lang('Auditorium.admin_permission_needed'));
            return redirect()->to('/auth/login');
        }
        helper(['form']);

        $data ['validation'] = \Config\Services::validation();
        echo view('classroom/import', $this->withIon($data));
    }

    /**
     * @throws \Exception
     */
    public function upload()
    {
        helper(['form', 'url']);

        if ($this->request->getMethod() === 'post' && $this->validate([
                'import' => 'uploaded[import]|ext_in[import,xml]|max_size[import,512]',
                'type' => 'required|exact_length[1]',
            ])) {
            if ($file = $this->request->getFile('import')) {
                //подключение хранилища
                $s3 = new S3Client([
                    'version' => 'latest',
                    'region' => 'us-east-1', //us-east-1
                    'endpoint' => getenv('S3_ENDPOINT'),
                    'use_path_style_endpoint' => true,
                    'credentials' => [
                        'key' => getenv('S3_KEY'), //чтение настроек окружения из файла .env
                        'secret' => getenv('S3_SECRET'), //чтение настроек окружения из файла .env
                    ],
                ]);
                $insert = null;
                //получение загруженного файла из HTTP-запроса
                if ($file->getSize() != 0) {
                    //загрузка файла в хранилище
                    $insert = $s3->putObject([
                        'Bucket' => getenv('S3_BUCKET'), //чтение настроек окружения из файла .env
                        //генерация случайного имени файла
                        'Key' => getenv('S3_KEY') . '/import.xml',
                        'Body' => fopen($file->getRealPath(), 'r+')
                    ]);
                }

                $flag = false;
                $model = new ClassroomModel();
                $type = $this->request->getPost('type');
                $xml = new \SimpleXMLElement($file->getRealPath(), NULL , true);

                //проверка структуры xml файла
                if(!$xml || $xml->count() < 1 )
                {
                    session()->setFlashdata('message', lang('Auditorium.root_xml_missing')); //FAILURE
                    return redirect()->to('/classroom/import');
                }
                foreach ($xml->children() as $room)
                {
                    if($room->count() != 14 && $room->count() != 15 && $room->count() != 16)
                    {
                        session()->setFlashdata('message', lang('Auditorium.wrong_xml_structure')); //FAILURE
                        return redirect()->to('/classroom/import');
                    }
                    if(((string) $room->building) == "" || ((string) $room->number) == "" || ((string) $room->type) == "" ||
                        ((string) $room->department_id) == "" || ((string) $room->area) == "" || ((string) $room->windows) == "" ||
                        ((string) $room->capacity) == "" || ((string) $room->smartboards) == "" || ((string) $room->stat_screens) == "" ||
                        ((string) $room->stat_projectors) == "" ||((string) $room->chalkboards) == "" || ((string) $room->computers) == "" ||
                        ((string) $room->move_projectors) == "" || ((string) $room->sinks) == "")
                    {
                        session()->setFlashdata('message', lang('Auditorium.wrong_xml_structure')); //FAILURE
                        return redirect()->to('/classroom/import');
                    }
                }
                //backup
                if ($type == 1)
                {
                    $model_v = new EquipmentNumValueModel();
                    if ($this->backup_extra($model_v))
                    {
                        $room = $model_v->getValues();
                        if (!empty($room))
                        {
                            if (!empty($model_v->countAll()))
                            {
                                foreach ($room as $key => $room_value)
                                    $model_v->delete($room_value['id_v']);
                            }
                            else
                                $model_v->delete($room['id_v']);
                        }
                    }
                    else
                    {
                        session()->setFlashdata('message', lang('Auditorium.backup_failure')); //FAILURE
                        return redirect()->to('/classroom/import');
                    }
                    if ($this->backup($model))
                    {
                        $room = $model->getClassroom();
                        if (!empty($room))
                        {
                            if (!empty($model->countAll()))
                            {
                                foreach ($room as $key => $room_value)
                                    $model->delete($room_value['id']);
                            }
                            else
                                $model->delete($room['id']);
                        }
                    }
                    else
                    {
                        session()->setFlashdata('message', lang('Auditorium.backup_failure')); //FAILURE
                        return redirect()->to('/classroom/import');
                    }

                    $flag = true;
                }

                //запись данных из xml в таблицу classroom
                foreach ($xml->children() as $room)
                {
                    try {
                        $data = [
                            'building' => (string) $room->building,
                            'number' => (string) $room->number,
                            'type' => (string) $room->type,
                            'department_id' => (string) $room->department_id,
                            'area' => (string) $room->area,
                            'windows' => (string) $room->windows,
                            'capacity' => (string) $room->capacity,
                            'smartboards' => (string) $room->smartboards,
                            'stat_screens' => (string) $room->stat_screens,
                            'stat_projectors' => (string) $room->stat_projectors,
                            'chalkboards' => (string) $room->chalkboards,
                            'computers' => (string) $room->computers,
                            'move_projectors' => (string) $room->move_projectors,
                            'sinks' => (string) $room->sinks,
                            'note' => (string) $room->note,
                        ];
                        if ( ((string) $room->picture_url) != "" )
                            $data['picture_url'] = $room->picture_url;
                    }
                    catch (Throwable $t)
                    {
                        session()->setFlashdata('message', lang('Auditorium.wrong_xml_data')); //FAILURE
                        return redirect()->to('/classroom/import');
                    }

                    if (!$type)
                    {
                        $id = $model->getId($data['building'], $data['number']);
                        if (empty($id))
                            $model ->save($data);
                        else
                            $model->update($id['id'], $data);
                    }
                    else
                        $model ->save($data);
                }
                if ($type == 0)
                    $flag = true;

                if (!is_null($insert) && $flag)
                {
                    session()->setFlashdata('message', lang('Auditorium.import_success')); //success
                    return redirect()->to('/classroom/import');
                }
            }
            else{
                session()->setFlashdata('message', lang('Auditorium.file_upload_failure')); //FAILURE
                return redirect()->to('/classroom/import');
            }

        } else
            return redirect()->to('/classroom/import')->withInput();
    }

    public function export()
    {
        if (!$this->ionAuth->loggedIn()) {
            return redirect()->to('/auth/login');
        }
        if (!$this->ionAuth->isAdmin()) {
            session()->setFlashdata('message', lang('Auditorium.admin_permission_needed'));
            return redirect()->to('/auth/login');
        }

        helper(['form']);

        $data ['validation'] = \Config\Services::validation();
        echo view('classroom/export', $this->withIon($data));
    }

    /**
     * @throws \PhpZip\Exception\ZipException
     */
    public function download()
    {
        helper(['form', 'url', 'filesystem']);
        if ($this->request->getMethod() === 'post' && $this->validate([
                'type' => 'required|exact_length[1]',
            ]))
        {
            $type = $this->request->getPost('type');
            $str = null;
            switch ($type)
            {
                default:
                    break;
                case 0:
                    $str .= 'building as Корпус, number as Аудитория, type as Назначение, area as Площадь, department.name as Подразделение_кафедра,
                             windows as Количество_окон, capacity as Количество_рабочих_посадочных_мест, smartboards as интерактивной_доски,
                             stat_screens as стационарного_экрана, stat_projectors as стационарного_проектора, chalkboards as меловой_доски,
                             computers as компьютеров, move_projectors as переносного_проектора, sinks as раковины, note as Примечание';
                    break;
                case 1:
                    $str .= 'building, number, type, area, department_id, windows, capacity, smartboards, stat_screens,
                             stat_projectors, chalkboards,computers, move_projectors, sinks, note';
                    break;
            }

            $model = new ClassroomModel();
            $db = \Closure::bind(function ($model) {
                return $model->db;
            }, null, $model)($model);

            $util = (new \CodeIgniter\Database\Database())->loadUtils($db);
            if(!is_null($str))
            {
                if ($type)
                    $util = $util->getXMLFromResult($model->select($str)->get());
                else
                    $util = $util->getXMLFromResult($model->select($str)->join('department','classroom.department_id = department.id_d')->get());
                return $this->response->download('export.xml', $util);
            }
            else {
                $classroom = null;
                $extra = null;
                $department = null;
                $equipment = null;
                $backup = null;

                if ($type == 3)
                {
                    $model_v = new EquipmentNumValueModel();
                    $model_d = new DepartmentModel();
                    $model_e = new EquipmentModel();
                    if ($this->backup($model) && $this->backup_extra($model_v) && $this->backup_department($model_d) && $this->backup_eqp($model_e))
                    {
                        $backup = true;
                    }
                    else{
                        session()->setFlashdata('message', lang('Auditorium.backup_failure'));
                        return redirect()->to('/classroom/export')->withInput();
                    }
                }

                //подключение хранилища
                $s3 = new S3Client([
                    'version' => 'latest',
                    'region' => 'us-east-1', //us-east-1
                    'endpoint' => getenv('S3_ENDPOINT'),
                    'use_path_style_endpoint' => true,
                    'credentials' => [
                        'key' => getenv('S3_KEY'), //чтение настроек окружения из файла .env
                        'secret' => getenv('S3_SECRET'), //чтение настроек окружения из файла .env
                    ],
                ]);

                //classroom
                if ($s3->doesObjectExist(getenv('S3_BUCKET'), getenv('S3_KEY') . '/export_classroom.xml'))
                {
                    //выгрузка файла из хранилища
                    $classroom = $s3->getObject([
                        'Bucket' => getenv('S3_BUCKET'), //чтение настроек окружения из файла .env
                        'Key' => getenv('S3_KEY') . '/export_classroom.xml',
                    ]);
                }
                else
                {
                    session()->setFlashdata('message', lang('Auditorium.backup_files_missing'));
                    return redirect()->to('/classroom/export')->withInput();
                }

                //extra_values
                if ($s3->doesObjectExist(getenv('S3_BUCKET'), getenv('S3_KEY') . '/export_extra.xml'))
                {
                    //выгрузка файла из хранилища
                    $extra = $s3->getObject([
                        'Bucket' => getenv('S3_BUCKET'), //чтение настроек окружения из файла .env
                        'Key' => getenv('S3_KEY') . '/export_extra.xml',
                    ]);
                }
                else
                {
                    session()->setFlashdata('message', lang('Auditorium.backup_files_missing'));
                    return redirect()->to('/classroom/export')->withInput();
                }

                //department
                if ($s3->doesObjectExist(getenv('S3_BUCKET'), getenv('S3_KEY') . '/export_department.xml'))
                {
                    //выгрузка файла из хранилища
                    $department = $s3->getObject([
                        'Bucket' => getenv('S3_BUCKET'), //чтение настроек окружения из файла .env
                        'Key' => getenv('S3_KEY') . '/export_department.xml',
                    ]);
                }
                else
                {
                    session()->setFlashdata('message', lang('Auditorium.backup_files_missing'));
                    return redirect()->to('/classroom/export')->withInput();
                }

                //equipment
                if ($s3->doesObjectExist(getenv('S3_BUCKET'), getenv('S3_KEY') . '/export_equipment.xml'))
                {
                    //выгрузка файла из хранилища
                    $equipment = $s3->getObject([
                        'Bucket' => getenv('S3_BUCKET'), //чтение настроек окружения из файла .env
                        'Key' => getenv('S3_KEY') . '/export_equipment.xml',
                    ]);
                }
                else
                {
                    session()->setFlashdata('message', lang('Auditorium.backup_files_missing'));
                    return redirect()->to('/classroom/export')->withInput();
                }

                if (!is_null($classroom) && !is_null($extra) && !is_null($department) && !is_null($equipment)) {
                    $entries = [
                        'export_classroom.xml' => $classroom['Body'], // add an entry from the string contents
                        'export_extra.xml' => $extra['Body'], // add an entry from the string contents
                        'export_department.xml' => $department['Body'], // add an entry from the string contents
                        'export_equipment.xml' => $equipment['Body'], // add an entry from the string contents
                    ];
                    $zipFile = new \PhpZip\ZipFile();
                    $zipFile->addAll($entries);
                    $zipFile->outputAsAttachment('backup.zip');
                    $zipFile->close();
                }
            }
        } else
            return redirect()->to('/classroom/export')->withInput();
    }

    private function backup(ClassroomModel $model)
    {
        if(empty($model))
            return false;
        $db = \Closure::bind(function ($model) {
            return $model->db;
        }, null, $model)($model);


        $util = (new \CodeIgniter\Database\Database())->loadUtils($db);
        $util = $util->getXMLFromResult($model->select('*')->get());

        $backup = null;

        //подключение хранилища
        $s3 = new S3Client([
            'version' => 'latest',
            'region' => 'us-east-1', //us-east-1
            'endpoint' => getenv('S3_ENDPOINT'),
            'use_path_style_endpoint' => true,
            'credentials' => [
                'key' => getenv('S3_KEY'), //чтение настроек окружения из файла .env
                'secret' => getenv('S3_SECRET'), //чтение настроек окружения из файла .env
            ],
        ]);

        //загрузка файла в хранилище
        $backup = $s3->putObject([
            'Bucket' => getenv('S3_BUCKET'), //чтение настроек окружения из файла .env
            //генерация случайного имени файла
            'Key' => getenv('S3_KEY') . '/export_classroom.xml',
            'Body' => $util,
        ]);

        if (is_null($backup))
        {
            session()->setFlashdata('message', lang('Auditorium.backup_failure')); //FAILURE
            return redirect()->to('/classroom');
        }
        return true;
    }
    private function backup_extra(EquipmentNumValueModel $model_v)
    {
        if(empty($model_v))
            return false;
        $db = \Closure::bind(function ($model) {
            return $model->db;
        }, null, $model_v)($model_v);


        $util = (new \CodeIgniter\Database\Database())->loadUtils($db);
        $util = $util->getXMLFromResult($model_v->select('*')->get());

        $backup = null;

        //подключение хранилища
        $s3 = new S3Client([
            'version' => 'latest',
            'region' => 'us-east-1', //us-east-1
            'endpoint' => getenv('S3_ENDPOINT'),
            'use_path_style_endpoint' => true,
            'credentials' => [
                'key' => getenv('S3_KEY'), //чтение настроек окружения из файла .env
                'secret' => getenv('S3_SECRET'), //чтение настроек окружения из файла .env
            ],
        ]);

        //загрузка файла в хранилище
        $backup = $s3->putObject([
            'Bucket' => getenv('S3_BUCKET'), //чтение настроек окружения из файла .env
            //генерация случайного имени файла
            'Key' => getenv('S3_KEY') . '/export_extra.xml',
            'Body' => $util,
        ]);

        if (is_null($backup))
        {
            session()->setFlashdata('message', lang('Auditorium.backup_failure')); //FAILURE
            return redirect()->to('/classroom');
        }
        return true;
    }
    private function backup_department(DepartmentModel $model_d)
    {
        if(empty($model_d))
            return false;
        $db = \Closure::bind(function ($model) {
            return $model->db;
        }, null, $model_d)($model_d);


        $util = (new \CodeIgniter\Database\Database())->loadUtils($db);
        $util = $util->getXMLFromResult($model_d->select('*')->get());

        $backup = null;

        //подключение хранилища
        $s3 = new S3Client([
            'version' => 'latest',
            'region' => 'us-east-1', //us-east-1
            'endpoint' => getenv('S3_ENDPOINT'),
            'use_path_style_endpoint' => true,
            'credentials' => [
                'key' => getenv('S3_KEY'), //чтение настроек окружения из файла .env
                'secret' => getenv('S3_SECRET'), //чтение настроек окружения из файла .env
            ],
        ]);

        //загрузка файла в хранилище
        $backup = $s3->putObject([
            'Bucket' => getenv('S3_BUCKET'), //чтение настроек окружения из файла .env
            //генерация случайного имени файла
            'Key' => getenv('S3_KEY') . '/export_department.xml',
            'Body' => $util,
        ]);

        if (is_null($backup))
        {
            session()->setFlashdata('message', lang('Auditorium.backup_failure')); //FAILURE
            return redirect()->to('/classroom');
        }
        return true;
    }
    private function backup_eqp(EquipmentModel $model_e)
    {
        if(empty($model_e))
            return false;
        $db = \Closure::bind(function ($model) {
            return $model->db;
        }, null, $model_e)($model_e);


        $util = (new \CodeIgniter\Database\Database())->loadUtils($db);
        $util = $util->getXMLFromResult($model_e->select('*')->get());

        $backup = null;

        //подключение хранилища
        $s3 = new S3Client([
            'version' => 'latest',
            'region' => 'us-east-1', //us-east-1
            'endpoint' => getenv('S3_ENDPOINT'),
            'use_path_style_endpoint' => true,
            'credentials' => [
                'key' => getenv('S3_KEY'), //чтение настроек окружения из файла .env
                'secret' => getenv('S3_SECRET'), //чтение настроек окружения из файла .env
            ],
        ]);

        //загрузка файла в хранилище
        $backup = $s3->putObject([
            'Bucket' => getenv('S3_BUCKET'), //чтение настроек окружения из файла .env
            //генерация случайного имени файла
            'Key' => getenv('S3_KEY') . '/export_equipment.xml',
            'Body' => $util,
        ]);

        if (is_null($backup))
        {
            session()->setFlashdata('message', lang('Auditorium.backup_failure')); //FAILURE
            return redirect()->to('/classroom');
        }
        return true;
    }

}