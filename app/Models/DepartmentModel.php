<?php namespace App\Models;
use CodeIgniter\Model;
class DepartmentModel extends Model
{
    protected $table = 'department'; //таблица, связанная с моделью

    //Перечень задействованных в модели полей таблицы
    protected $allowedFields = ['name'];

    //Первичный ключ
    protected $primaryKey = "id_d";

    public function getDepartment($id = null)
    {
        if (!isset($id)) {
            return $this->findAll();
        }
        return $this->where(['id_d' => $id])->first();

    }
}
