<?php namespace App\Models;
use CodeIgniter\Model;
class EquipmentNumValueModel extends Model
{
    protected $table = 'extra_values'; //таблица, связанная с моделью

    //Перечень задействованных в модели полей таблицы
    protected $allowedFields = ['id_v', 'id_eqp', 'id_room', 'value'];

    //Первичный ключ
    protected $primaryKey = "id_v";

    public function getValues($id = null)
    {
        if (!isset($id)) {
            return $this->findAll();
        }
        return $this->where(['id' => $id])->first();
    }

    public function getValuesByRoom($id = null)
    {
        if (!is_null($id)) {
            return $this->where(['id_room' => $id])->findAll();
        }
        return $this;
    }

    public function getValuesWithEquipment($id = null)
    {
        $builder = $this->select('*')->join('extra_equipment','extra_values.id_eqp = extra_equipment.id');
        if (!is_null($id))
        {
            $builder->where(['id_room' => $id]);
        }
        return $builder->findAll();
    }

    public function getValuesBySearch($searchEqp = null, $searchValue = null)
    {
        if (!is_null($searchEqp) && !is_null($searchValue))
        {
            $builder = $this->select('id_room')->where(['id_eqp' => $searchEqp]);
            return $builder->where('value >=', $searchValue)->findAll();
        }
        return $this;
    }
}
