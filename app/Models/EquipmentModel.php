<?php namespace App\Models;
use CodeIgniter\Model;
use App\Models\EquipmentNumValueModel;
class EquipmentModel extends Model
{
    protected $table = 'extra_equipment'; //таблица, связанная с моделью

    //Перечень задействованных в модели полей таблицы
    protected $allowedFields = ['id', 'name'];

    public function getEquipment($id = null)
    {
        if (!isset($id)) {
            return $this->findAll();
        }
        return $this->where(['id' => $id])->first();
    }

    public function getEquipmentWithoutRoomValues($id = null)
    {
        $builder = $this;
        if (!is_null($id))
        {
            $model = new EquipmentNumValueModel();
            $model = $model->getValuesByRoom($id);
            if (!empty($model))
            {
                $room_value = array();
                foreach ($model as $item => $value)
                    $room_value[] = $value['id_eqp'];
                if (!empty($room_value))
                    $builder = $builder->whereNotIn('id', $room_value);
            }
        }
        return $builder->findAll();
    }

    public function getEquipmentId($id = null)
    {
        if (!isset($id)) {
            return $this->select('id')->findAll();
        }
        return $this->select('id')->where(['id' => $id])->first();
    }
}