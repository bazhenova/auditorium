<?php namespace App\Models;
use CodeIgniter\Model;
use App\Models\EquipmentNumValueModel;
class ClassroomModel extends Model
{
    protected $table = 'classroom'; //таблица, связанная с моделью

    //Перечень задействованных в модели полей таблицы
    protected $allowedFields = ['building', 'number', 'type', 'department_id',
                                'area', 'windows', 'capacity', 'smartboards',
                                'stat_screens', 'stat_projectors', 'chalkboards',
                                'computers', 'move_projectors', 'sinks', 'note', 'picture_url'];

    public function getClassroom($id = null)
    {
        if (!isset($id)) {
            return $this->findAll();
        }
        return $this->where(['id' => $id])->first();
    }

    public function getClassroomByPage($id = null)
    {
        if (!isset($id))
            $builder=$this;
        else
            $builder=$this->where(['id' => $id]);
        return $builder;
    }

    public function getClassroomBySearch($searchEqp, $id = null, $searchBuilding = '', $searchType = '', $searchDepartment_id = '', $searchArea = '0',
                                         $searchWindows = '', $searchCapacity = '', $searchSmartboards = '', $searchStat_screens = '',
                                         $searchStat_projectors = '', $searchChalkboards = '', $searchComputers = '',
                                         $searchMove_projectors = '', $searchSinks = '')
    {
        $builder = $this->where('area >=', $searchArea);
        $builder = $builder->where('windows >=', $searchWindows);
        $builder = $builder->where('capacity >=', $searchCapacity);
        $builder = $builder->where('smartboards >=', $searchSmartboards);
        $builder = $builder->where('stat_screens >=', $searchStat_screens);
        $builder = $builder->where('stat_projectors >=', $searchStat_projectors);
        $builder = $builder->where('chalkboards >=', $searchChalkboards);
        $builder = $builder->where('computers >=', $searchComputers);
        $builder = $builder->where('move_projectors >=', $searchMove_projectors);
        $builder = $builder->where('sinks >=', $searchSinks);
        if ($searchBuilding != '0')
            $builder = $builder->where('building', $searchBuilding);
        if ($searchDepartment_id != '0')
            $builder = $builder->where('department_id', $searchDepartment_id);
        if ($searchType != '0')
            $builder = $builder->where('type', $searchType);

        if (!empty($searchEqp))
        {
            foreach ($searchEqp as $key => $value)
            {
                $model_v = new EquipmentNumValueModel();
                $model_v = $model_v->getValuesBySearch($value['id_eqp'], $value['value']);
                if (!empty($model_v))
                {
                    $room = array();
                    foreach ($model_v as $k => $val)
                        $room [] = $val['id_room'];
                    if (!empty($room))
                        $builder = $builder->whereIn('id', $room);
                }
                else
                    return $builder->where('id', '0')->findAll();
            }
        }

        if (!is_null($id)) {
            return $builder->where(['id' => $id])->first();
        }
        return $builder->findAll();
    }

    public function getClassroomWithDepartment($id = null)
    {
        $builder = $this->select('*')->join('department','classroom.department_id = department.id_d');
        if (!is_null($id))
        {
            return $builder->where(['id' => $id])->first();
        }
        return $builder->findAll();
    }

    public function getLastId()
    {
        return $this->selectMax('id')->get()->getResult('array');
    }

    public function getId($building = null, $number = null)
    {
        if (!isset($building) || !isset($number)) {
            return $this->where('id', '0')->findAll();
        }
        $builder = $this->select('id')->where('building', $building);
        return $builder->where('number', $number)->first();
    }

}
