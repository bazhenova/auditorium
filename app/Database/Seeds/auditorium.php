<?php namespace App\Database\Seeds;

use CodeIgniter\Database\Seeder;

class auditorium extends Seeder
{
    public function run()
    {
        //classrooms
        /*
            $data = [
                'id' => '',
                'building' => '',
                'number' => '',
                'type' => '',
                'department_id' => '',
                'area' => '',
                'windows' => '',
                'capacity' => '',
                'smartboards' => '',
                'stat_screens' => '',
                'stat_projectors' => '',
                'chalkboards' => '',
                'computers' => '',
                'move_projectors' => '',
                'sinks' => '',
                'note' => '',
            ];
            $this->db->table('classroom')->insert($data);
        */

        $data = [
            'id' => '1',
            'building' => 'А',
            'number' => '320',
            'type' => 'км',
            'department_id' => '1',
            'area' => '37.0',
            'windows' => '2',
            'capacity' => '13',
            'smartboards' => '0',
            'stat_screens' => '0',
            'stat_projectors' => '0',
            'chalkboards' => '1',
            'computers' => '13',
            'move_projectors' => '1',
            'sinks' => '1',
            'note' => '',
        ];
        $this->db->table('classroom')->insert($data);

        $data = [
            'id' => '2',
            'building' => 'У',
            'number' => '105',
            'type' => 'лб',
            'department_id' => '1',
            'area' => '37.1',
            'windows' => '0',
            'capacity' => '13',
            'smartboards' => '0',
            'stat_screens' => '1',
            'stat_projectors' => '0',
            'chalkboards' => '1',
            'computers' => '9',
            'move_projectors' => '0',
            'sinks' => '1',
            'note' => '',
        ];
        $this->db->table('classroom')->insert($data);

        $data = [
            'id' => '3',
            'building' => 'У',
            'number' => '106',
            'type' => 'лб',
            'department_id' => '1',
            'area' => '37.1',
            'windows' => '0',
            'capacity' => '13',
            'smartboards' => '0',
            'stat_screens' => '1',
            'stat_projectors' => '0',
            'chalkboards' => '1',
            'computers' => '1',
            'move_projectors' => '0',
            'sinks' => '1',
            'note' => '',
        ];
        $this->db->table('classroom')->insert($data);

        $data = [
            'id' => '4',
            'building' => 'У',
            'number' => '401',
            'type' => 'лб',
            'department_id' => '1',
            'area' => '34.6',
            'windows' => '2',
            'capacity' => '21',
            'smartboards' => '0',
            'stat_screens' => '1',
            'stat_projectors' => '0',
            'chalkboards' => '1',
            'computers' => '1',
            'move_projectors' => '0',
            'sinks' => '1',
            'note' => '',
        ];
        $this->db->table('classroom')->insert($data);

        //departments
        /*
            $data = [
                'id_d'=> '',
                'name'=> '',
                'faculty_number'=> '',
            ];
            $this->db->table('department')->insert($data);
        */

        $data = [
            'id_d'=> '1',
            'name'=> 'ПИ/Автоматики и компьютерных систем',
            'faculty_number'=> '24',
        ];
        $this->db->table('department')->insert($data);

        $data = [
            'id_d'=> '2',
            'name'=> 'ИГОИС/Иностранных языков',
            'faculty_number'=> '4',
        ];
        $this->db->table('department')->insert($data);

        $data = [
            'id_d'=> '3',
            'name'=> 'ИЭИУ/Менеджмента и бизнеса',
            'faculty_number'=> '17',
        ];
        $this->db->table('department')->insert($data);
    }
}