<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class Auditorium extends Migration
{
	public function up()
	{
        //classroom
        if (!$this->db->tableexists('classroom'))
        {
            // Setup Keys
            $this->forge->addkey('id', TRUE);
            // Build Schema
            $this->forge->addfield(array(
                'id' => array('type' => 'INT', 'unsigned' => TRUE, 'null' => FALSE, 'auto_increment' => TRUE),
                'building' => array('type' => 'VARCHAR', 'constraint' => '1', 'null' => FALSE),
                'number' => array('type' => 'VARCHAR', 'constraint' => '5', 'null' => FALSE),
                'type' => array('type' => 'VARCHAR', 'constraint' => '2', 'null' => FALSE),
                'department_id' => array('type' => 'INT', 'unsigned' => TRUE, 'null' => FALSE),
                'area' => array('type' => 'NUMERIC', 'constraint' => '5,1', 'default' => '0', 'null' => FALSE),
                'windows' => array('type' => 'INT', 'unsigned' => TRUE, 'default' => '0', 'null' => FALSE),
                'capacity' => array('type' => 'INT', 'unsigned' => TRUE, 'default' => '0', 'null' => FALSE),
                'smartboards' => array('type' => 'INT', 'unsigned' => TRUE, 'default' => '0', 'null' => FALSE),
                'stat_screens' => array('type' => 'INT', 'unsigned' => TRUE, 'default' => '0', 'null' => FALSE),
                'stat_projectors' => array('type' => 'INT', 'unsigned' => TRUE, 'default' => '0', 'null' => FALSE),
                'chalkboards' => array('type' => 'INT', 'unsigned' => TRUE, 'default' => '0', 'null' => FALSE),
                'computers' => array('type' => 'INT', 'unsigned' => TRUE, 'default' => '0', 'null' => FALSE),
                'move_projectors' => array('type' => 'INT', 'unsigned' => TRUE, 'default' => '0', 'null' => FALSE),
                'sinks' => array('type' => 'INT', 'unsigned' => TRUE, 'default' => '0', 'null' => FALSE),
                'note' => array('type' => 'VARCHAR', 'constraint' => '255', 'default' => 'null', 'null' => TRUE)
            ));
            // Create table
            $this->forge->createtable('classroom', TRUE);
        }

        //department
        if (!$this->db->tableexists('department'))
        {
            // Setup Keys
            $this->forge->addkey('id_d', TRUE);
            // Build Schema
            $this->forge->addfield(array(
                'id_d' => array('type' => 'INT', 'unsigned' => TRUE, 'null' => FALSE, 'auto_increment' => TRUE),
                'name' => array('type' => 'VARCHAR', 'constraint' => '255', 'null' => FALSE),
                'faculty_number' => array('type' => 'INT', 'unsigned' => TRUE, 'null' => TRUE)
            ));
            // Create table
            $this->forge->createtable('department', TRUE);
        }
	}

	public function down()
	{
		//
        $this->forge->dropTable('classroom');
        $this->forge->dropTable('department');
	}
}
