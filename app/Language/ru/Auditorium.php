<?php
return [
        'classroom_create_success' => 'Аудитория была успешно создана',
        'classroom_update_success' => 'Аудитория была успешно обновлена',
        'classroom_delete_success' => 'Аудитория была успешно удалена',
        'login_with_google' => 'Войти с Google',
        'field_create_success' => 'Поле было успешно создано',
        'field_delete_success' => 'Поле было успешно удалено',
        'admin_permission_needed' => 'Требуются права администратора',
        'root_xml_missing' => 'Отсутствует корневой элемент',
        'wrong_xml_structure' => 'Отсутствуют требуемые элементы в XML файле',
        'wrong_xml_data' => 'Ошибка чтения данных из XML файла',
        'backup_failure' => 'Ошибка при создании резервной копии',
        'import_success' => 'Импорт был успешно выполнен',
        'file_upload_failure' => 'Ошибка при загрузке файла',
        'backup_files_missing' => 'Резервные копии отсутствуют',
        'already_existed' => 'Такая аудитория уже существует',


];
