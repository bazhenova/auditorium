<?= $this->extend('templates/layout') ?>
<?= $this->section('content') ?>
<div class="jumbotron jumbotron-fluid text-center">
    <!img class="mb-4" src="https://www.flaticon.com/svg/static/icons/svg/2422/2422618.svg" alt="" width="72" height="72"><h1 class="display-4">Учет аудиторного фонда университета</h1>
    <p class="lead">Это приложение помогает осуществлять поиск и учет аудиторий.</p>
    <?php if (! $ionAuth->loggedIn()): ?>
        <a class="btn btn-primary btn-lg" href="<?= base_url()?>/auth/login">Вход</a>
    <?php endif ?>
</div>
<?= $this->endSection() ?>
