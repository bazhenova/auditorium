<?= $this->extend('templates/layout') ?>
<?= $this->section('content') ?>
    <div class="container main">
        <h2>Все аудитории</h2>

        <div class="d-flex justify-content-between mb-2">
            <?= $pager->links('group1','my_page') ?>
            <?= form_open('classroom/index', ['style' => 'display: flex']); ?>
            <select name="per_page" class="ml-3" aria-label="per_page">
                <option value="2" <?php if($per_page == '2') echo("selected"); ?>>2</option>
                <option value="5"  <?php if($per_page == '5') echo("selected"); ?>>5</option>
                <option value="10" <?php if($per_page == '10') echo("selected"); ?>>10</option>
                <option value="20" <?php if($per_page == '20') echo("selected"); ?>>20</option>
            </select>
            <button class="btn btn-outline-success" type="submit" class="btn btn-primary">На странице</button>
            </form>
        </div>
        <?php if (!empty($classroom) && is_array($classroom)) : ?>

            <?php foreach ($classroom as $item): ?>

                <div class="card mb-3" style="max-width: 540px;">
                    <div class="row">
                        <div class="col-md-4 d-flex align-items-center">
                            <?php if (is_null($item['picture_url'])) : ?>
                                <img height="150" src="https://www.flaticon.com/svg/static/icons/svg/2422/2422514.svg" class="card-img" alt="<?= esc($item['building']); ?><?= esc($item['number']); ?>">
                            <?php else:?>
                                <img height="150" src="<?= esc($item['picture_url']); ?>" class="card-img" alt="<?= esc($item['building']); ?><?= esc($item['number']); ?>">
                            <?php endif ?>
                        </div>
                        <div class="col-md-8">
                            <div class="card-body">
                                <h5 class="card-title">Аудитория <?= esc($item['building']); ?><?= esc($item['number']); ?></h5>
                                <a href="<?= base_url()?>/index.php/classroom/view/<?= esc($item['id']); ?>" class="btn btn-primary">Подробнее</a>
                            </div>
                        </div>
                    </div>
                </div>

            <?php endforeach; ?>
        <?php else : ?>
            <p>Невозможно найти аудитории.</p>
        <?php endif ?>
    </div>
<?= $this->endSection() ?>