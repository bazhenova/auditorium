<?= $this->extend('templates/layout') ?>
<?= $this->section('content') ?>
    <div class="container main">
        <h2>Поиск аудиторий</h2>
        <?= form_open('classroom/search',['style' => 'display: flex']); ?>
        <div class="card text-dark bg-light ml-0">
            <div class="card-body">
                <form>
                    <div class="row g-3 pb-3">
                        <div class="col-md-3">
                            <label for="inputBuilding" class="form-label">Корпус</label>
                            <select id="inputBuilding" class="form-select form-control" size="1" name="building">
                                <option value="0">Любой корпус</option>
                                <option value="А">А</option>
                                <option value="Г">Г</option>
                                <option value="К">К</option>
                                <option value="С">С</option>
                                <option value="У">У</option>
                            </select>
                        </div>
                        <div class="col-md-3">
                            <label for="inputState" class="form-label">Назначение</label>
                            <select id="inputState" class="form-select form-control" size="1" name="type">
                                <option value="0">Любой тип аудитории</option>
                                <option value="лб">лб</option>
                                <option value="км">км</option>
                                <option value="нл">нл</option>
                                <option value="пд">пд</option>
                                <option value="пп">пп</option>
                                <option value="пр">пр</option>
                            </select>
                        </div>
                        <div class="col-md-4">
                            <label for="inputState" class="form-label">Подразделение/кафедра</label>
                            <select id="inputState" class="form-select form-control" size="1" name="department_id">
                                <option value="0">Любое подразделение/кафедра</option>
                                <?php
                                foreach ($department as $value) {
                                    echo "<option value = {$value['id_d']} > {$value['name']} </option>";}
                                ?>
                            </select>
                        </div>
                        <div class="col-md-2">
                            <label for="inputArea" class="form-label">Площадь</label>
                            <input type="text" min="0" class="form-control" id="inputArea" name="area" aria-label="Search"
                                   value="<?= $searchArea; ?>" required>
                        </div>
                    </div>
                    <div class="col-md-auto text-center">
                        <label class="form-label font-weight-bold">Количество/наличие:</label>
                    </div>
                    <div class="row g-3 pb-3">
                        <div class="col-md-4">
                            <div class="input-group">
                                <div class="input-group-text">окон</div>
                                <input type="number" min="0" class="form-control" id="inputWindows" name="windows" aria-label="Search"
                                       value="<?= $searchWindows; ?>" required>
                            </div>
                            <div class="input-group">
                                <div class="input-group-text">посадочных/рабочих мест</div>
                                <input type="number" min="0" class="form-control" id="inputCapacity" name="capacity" aria-label="Search"
                                       value="<?= $searchCapacity; ?>" required>
                            </div>
                            <div class="input-group">
                                <div class="input-group-text">компьютеров</div>
                                <input type="number" min="0" class="form-control" id="inputComputers" name="computers" aria-label="Search"
                                       value="<?= $searchComputers; ?>" required>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="input-group">
                                <div class="input-group-text">меловой доски</div>
                                <input type="number" min="0" class="form-control" id="inputChalkboards" name="chalkboards" aria-label="Search"
                                       value="<?= $searchChalkboards; ?>" required>
                            </div>
                            <div class="input-group">
                                <div class="input-group-text">раковин</div>
                                <input type="number" min="0" class="form-control" id="inputSinks" name="sinks" aria-label="Search"
                                       value="<?= $searchSinks; ?>" required>
                            </div>
                            <div class="input-group">
                                <div class="input-group-text">интерактивного экранов</div>
                                <input type="number" min="0" class="form-control" id="inputSmartboards" name="smartboards" aria-label="Search"
                                       value="<?= $searchSmartboards; ?>" required>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="input-group">
                                <div class="input-group-text">стационарного экранов</div>
                                <input type="number" min="0" class="form-control" id="inputStat_screens" name="stat_screens" aria-label="Search"
                                       value="<?= $searchStat_screens; ?>" required>
                            </div>
                            <div class="input-group">
                                <div class="input-group-text">стационарного проектора</div>
                                <input type="number" min="0" class="form-control" id="inputStat_projectors" name="stat_projectors" aria-label="Search"
                                       value="<?= $searchStat_projectors; ?>" required>
                            </div>
                            <div class="input-group">
                                <div class="input-group-text">переносного проектора</div>
                                <input type="number" min="0" class="form-control" id="inputMove_projectors" name="move_projectors" aria-label="Search"
                                       value="<?= $searchMove_projectors; ?>" required>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-4">
                    <?php if (!empty($equipment)): ?>
                        <?php foreach ($equipment as $item): ?>
                            <div class="input-group">
                                <div class="input-group-text"><?= esc($item['name']); ?></div>
                                <input type="number" min="0" class="form-control" name="<?= esc($item['id']); ?>" value="0" required>
                            </div>
                        <?php endforeach; ?>
                    <?php endif ?>
                    </div>

                    <div class="col-md-auto text-center">
                        <label class="form-label">Примечание: в полях для характеристик/оборудования, которые не требуются для поиска, следует оставить 0.</label>
                    </div>
                    <div class="col-auto text-center">
                        <button type="submit" class="btn btn-primary btn-lg">Найти аудитории</button>
                    </div>
                </form>
            </div>
        </div>

        <div class="table-responsive">
            <table class="table table-striped">
                <thead>
                <th scope="col">Корпус</th>
                <th scope="col">Номер</th>
                <th scope="col">Назначение</th>
                <th scope="col">Количество рабочих мест</th>
                <th scope="col">Площадь</th>
                <th scope="col">Управление</th>
                </thead>
            <?php if (!empty($classroom) && is_array($classroom)) : ?>
                <tbody>
                <?php foreach ($classroom as $item): ?>
                    <tr>
                        <td><?= esc($item['building']); ?></td>
                        <td><?= esc($item['number']); ?></td>
                        <td><?= esc($item['type']); ?></td>
                        <td><?= esc($item['capacity']); ?></td>
                        <td><?= esc($item['area']); ?> кв.м</td>
                        <td>
                            <a href="<?= base_url()?>/classroom/view/<?= esc($item['id']); ?>" class="btn btn-primary btn-sm">Подробнее</a>
                        </td>
                    </tr>
                <?php endforeach; ?>
                </tbody>
            </table>
            <?php else : ?>
                </table>
                <div class="text-center">
                    <p>Аудитории не найдены </p>
                </div>
            <?php endif ?>
        </div>
    </div>
<?= $this->endSection() ?>