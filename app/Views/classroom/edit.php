<?= $this->extend('templates/layout') ?>
<?= $this->section('content') ?>
    <div class="container" style="max-width: 540px;">
        <?= form_open_multipart('classroom/update'); ?>
        <input type="hidden" name="id" value="<?= $classroom["id"] ?>">
        <div class="form-group">
            <p class="text-center font-weight-bold">В полях, отмеченных * при отсутствии оборудования или парметров характеристик следует ввести 0.</p>
        </div>
        <div class="form-group">
            <label for="building">Корпус</label>
            <input type="text" class="form-control <?= ($validation->hasError('building')) ? 'is-invalid' : ''; ?>" name="building"
                   value="<?= $classroom["building"]; ?>" disabled>
            <div class="invalid-feedback">
                <?= $validation->getError('building') ?>
            </div>
        </div>
        <div class="form-group">
            <label for="number">Номер</label>
            <input type="text" class="form-control <?= ($validation->hasError('number')) ? 'is-invalid' : ''; ?>" name="number"
                   value="<?= $classroom["number"]; ?>" disabled>
            <div class="invalid-feedback">
                <?= $validation->getError('number') ?>
            </div>
        </div>
        <div class="form-group">
            <label for="type">Назначение</label>
            <select class="form-select form-control <?= ($validation->hasError('type')) ? 'is-invalid' : ''; ?>" size="1" name="type" required>
                <option disabled>Выберите тип аудитории</option>
                <option value="лб" <?php if ($classroom["type"] == 'лб') echo "selected"; ?>>лб</option>
                <option value="км" <?php if ($classroom["type"] == 'км') echo "selected"; ?>>км</option>
                <option value="нл" <?php if ($classroom["type"] == 'нл') echo "selected"; ?>>нл</option>
                <option value="пд" <?php if ($classroom["type"] == 'пд') echo "selected"; ?>>пд</option>
                <option value="пп" <?php if ($classroom["type"] == 'пп') echo "selected"; ?>>пп</option>
                <option value="пр" <?php if ($classroom["type"] == 'пр') echo "selected"; ?>>пр</option>
            </select>
            <div class="invalid-feedback">
                <?= $validation->getError('type') ?>
            </div>
        </div>
        <div class="form-group">
            <label for="department_id">Подразделение/кафедра</label>
            <select class="form-select form-control" size="1" name="department_id" required>
                <option selected disabled>Выберите подразделение/кафедру</option>
                <?php
                foreach ($department as $value) {
                    echo "<option value = {$value['id_d']} ";
                    if ($classroom["department_id"] == $value['id_d'])
                        echo "selected";
                    echo"> {$value['name']} </option>";}
                ?>
            </select>
            <div class="invalid-feedback">
                <?= $validation->getError('department_id') ?>
            </div>
        </div>
        <div class="form-group">
            <label for="area">Площадь, кв. м</label>
            <input type="text" class="form-control <?= ($validation->hasError('area')) ? 'is-invalid' : ''; ?>" name="area"
                   value="<?= $classroom["area"]; ?>">
            <div class="invalid-feedback">
                <?= $validation->getError('area') ?>
            </div>
        </div>
        <div class="form-group">
            <label for="windows">Количество окон</label>
            <input type="number" class="form-control <?= ($validation->hasError('windows')) ? 'is-invalid' : ''; ?>" name="windows"
                   value="<?= $classroom["windows"]; ?>">
            <div class="invalid-feedback">
                <?= $validation->getError('windows') ?>
            </div>
        </div>
        <div class="form-group">
            <label for="capacity">Количество рабочих мест</label>
            <input type="number" class="form-control <?= ($validation->hasError('capacity')) ? 'is-invalid' : ''; ?>" name="capacity"
                   value="<?= $classroom["capacity"]; ?>">
            <div class="invalid-feedback">
                <?= $validation->getError('capacity') ?>
            </div>
        </div>
        <div class="form-group">
            <label for="smartboards">Наличие интерактивной доски</label>
            <input type="number" class="form-control <?= ($validation->hasError('smartboards')) ? 'is-invalid' : ''; ?>" name="smartboards"
                   value="<?= $classroom["smartboards"]; ?>">
            <div class="invalid-feedback">
                <?= $validation->getError('smartboards') ?>
            </div>
        </div>
        <div class="form-group">
            <label for="stat_screens">Наличие стационарного экрана</label>
            <input type="number" class="form-control <?= ($validation->hasError('stat_screens')) ? 'is-invalid' : ''; ?>" name="stat_screens"
                   value="<?= $classroom["stat_screens"]; ?>">
            <div class="invalid-feedback">
                <?= $validation->getError('stat_screens') ?>
            </div>
        </div>
        <div class="form-group">
            <label for="stat_projectors">Наличие стационарного проектора</label>
            <input type="number" class="form-control <?= ($validation->hasError('stat_projectors')) ? 'is-invalid' : ''; ?>" name="stat_projectors"
                   value="<?= $classroom["stat_projectors"]; ?>">
            <div class="invalid-feedback">
                <?= $validation->getError('stat_projectors') ?>
            </div>
        </div>
        <div class="form-group">
            <label for="chalkboards">Наличие меловой доски</label>
            <input type="number" class="form-control <?= ($validation->hasError('chalkboards')) ? 'is-invalid' : ''; ?>" name="chalkboards"
                   value="<?= $classroom["chalkboards"]; ?>">
            <div class="invalid-feedback">
                <?= $validation->getError('chalkboards') ?>
            </div>
        </div>
        <div class="form-group">
            <label for="computers">Наличие компьютеров</label>
            <input type="number" class="form-control <?= ($validation->hasError('computers')) ? 'is-invalid' : ''; ?>" name="computers"
                   value="<?= $classroom["computers"]; ?>">
            <div class="invalid-feedback">
                <?= $validation->getError('computers') ?>
            </div>
        </div>
        <div class="form-group">
            <label for="move_projectors">Наличие переносного проектора</label>
            <input type="number" class="form-control <?= ($validation->hasError('move_projectors')) ? 'is-invalid' : ''; ?>" name="move_projectors"
                   value="<?= $classroom["move_projectors"]; ?>">
            <div class="invalid-feedback">
                <?= $validation->getError('move_projectors') ?>
            </div>
        </div>
        <div class="form-group">
            <label for="sinks">Наличие раковины</label>
            <input type="number" class="form-control <?= ($validation->hasError('sinks')) ? 'is-invalid' : ''; ?>" name="sinks"
                   value="<?= $classroom["sinks"]; ?>">
            <div class="invalid-feedback">
                <?= $validation->getError('sinks') ?>
            </div>
        </div>
        <?php if (!empty($values)): ?>
            <?php foreach ($values as $item): ?>
                <div class="form-group">
                    <label for="<?= esc($item['id_eqp']); ?>"><?= esc($item['name']); ?></label>
                    <input type="number" min="0" class="form-control" name="<?= esc($item['id_eqp']); ?>"
                           value="<?= $item['value']; ?>" required>
                </div>
            <?php endforeach; ?>
        <?php endif ?>

        <?php if (!empty($equipment)): ?>
            <?php foreach ($equipment as $item): ?>
                <div class="form-group">
                    <label for="<?= esc($item['id']); ?>"><?= esc($item['name']); ?></label>
                    <input type="number" min="0" class="form-control" name="<?= esc($item['id']); ?>">
                </div>
            <?php endforeach; ?>
        <?php endif ?>
        <div class="form-group">
            <label for="note">Примечание</label>
            <textarea class="form-control <?= ($validation->hasError('note')) ? 'is-invalid' : ''; ?>" name="note"
                      value="<?= $classroom["note"]; ?>">
            </textarea>
            <div class="invalid-feedback">
                <?= $validation->getError('note') ?>
            </div>
        </div>
        <div class="form-group">
            <label for="picture">Изображение</label>
            <input type="file" class="form-control-file <?= ($validation->hasError('picture')) ? 'is-invalid' : ''; ?>" name="picture">
            <div class="invalid-feedback">
                <?= $validation->getError('picture') ?>
            </div>
        </div>
        <div class="form-group">
            <button type="submit" class="btn btn-primary" name="submit">Сохранить</button>
        </div>
        </form>
    </div>
<?= $this->endSection() ?>