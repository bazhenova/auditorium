<?= $this->extend('templates/layout') ?>
<?= $this->section('content') ?>
    <div class="container" style="max-width: 540px;">
        <h2>Экспорт</h2>
        <?= form_open_multipart('classroom/download'); ?>
        <div class="form-group">
            <label for="type">Выберите формат экспорта</label>
            <select class="form-select form-control <?= ($validation->hasError('type')) ? 'is-invalid' : ''; ?>" size="1" name="type" required>
                <option value="0">XML для Excel</option>
                <option value="1">XML для импорта</option>
                <option value="2">XML последней резервной копии</option>
                <option value="3">XML новой резервной копии</option>
            </select>
            <div class="invalid-feedback">
                <?= $validation->getError('type') ?>
            </div>
        </div>
        <div class="form-group">
            <button type="submit" class="btn btn-primary" name="submit">Экспортировать</button>
        </div>
        </form>
    </div>
<?= $this->endSection() ?>
