<?= $this->extend('templates/layout') ?>
<?= $this->section('content') ?>
    <div class="container" style="max-width: 540px;">
        <h2>Импорт</h2>
        <?= form_open_multipart('classroom/upload'); ?>
        <div class="form-group">
            <label for="type">Выберите вариант импорта</label>
            <select class="form-select form-control <?= ($validation->hasError('type')) ? 'is-invalid' : ''; ?>" size="1" name="type" required>
                <option value="0">Добавление данных в таблицу</option>
                <option value="1">Полное обновление таблицы (с удалением)</option>
            </select>
            <div class="invalid-feedback">
                <?= $validation->getError('type') ?>
            </div>
        </div>
        <div class="form-group">
            <label for="import">Файл импорта в формате XML</label>
            <input type="file" class="form-control-file <?= ($validation->hasError('import')) ? 'is-invalid' : ''; ?>" name="import" id="import">
            <div class="invalid-feedback">
                <?= $validation->getError('import') ?>
            </div>
        </div>
        <div class="form-group">
            <button type="submit" class="btn btn-primary" name="submit">Импортировать</button>
        </div>
        </form>
    </div>
<?= $this->endSection() ?>
