<?= $this->extend('templates/layout') ?>
<?= $this->section('content') ?>
<div class="container" style="max-width: 540px;">
    <h2>Добавление новых активов аудиторий</h2>
    <?= form_open('classroom/add'); ?>
    <div class="form-group">
        <label for="name">Название нового актива:</label>
        <input type="text" class="form-control <?= ($validation->hasError('name')) ? 'is-invalid' : ''; ?>" name="name" id="name"
               value="<?= old('name'); ?>" placeholder="Введите название нового актива">
        <div class="invalid-feedback">
            <?= $validation->getError('name') ?>
        </div>
    </div>
    <div class="form-group">
        <button type="submit" class="btn btn-primary" name="submit">Добавить</button>
    </div>
</div>
<?= $this->endSection() ?>
