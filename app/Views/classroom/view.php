<?= $this->extend('templates/layout') ?>
<?= $this->section('content') ?>
    <div class="container main">
        <?php use CodeIgniter\I18n\Time; ?>
        <?php if (!empty($classroom)) : ?>
            <div class="card mb-3" style="max-width: 540px;">
                <div class="row">
                    <div class="col-md-4 d-flex align-items-center">
                        <?php if (is_null($classroom['picture_url'])) : ?>
                            <img height="150" src="https://www.flaticon.com/svg/static/icons/svg/2422/2422514.svg" class="card-img" alt="<?= esc($classroom['building']); ?><?= esc($classroom['number']); ?>">
                        <?php else:?>
                            <img height="150" src="<?= esc($classroom['picture_url']); ?>" class="card-img" alt="<?= esc($classroom['building']); ?><?= esc($classroom['number']); ?>">
                        <?php endif ?>
                    </div>
                    <div class="col-md-8">
                        <div class="card-body">
                            <h5 class="card-title">Аудитория <?= esc($classroom['building']); ?><?= esc($classroom['number']); ?></h5>
                            <div class="d-flex justify-content-between">
                                <div class="my-0">Назначение: </div>
                                <div class="text-muted"><?= esc($classroom['type']); ?></div>
                            </div>
                            <div class="d-flex justify-content-between">
                                <div class="my-0">Площадь, кв. м:</div>
                                <span class="text-muted"><?= esc($classroom['area']); ?></span>
                            </div>
                            <div class="d-flex justify-content-between">
                                <div class="my-0">Подразделение/кафедра: </div>
                                <div class="text-muted text-right" ><?= esc($classroom['name']); ?></div>
                            </div>
                            <div class="d-flex justify-content-between">
                                <div class="my-0">Количество посадочных мест: </div>
                                <span class="text-muted"><?= esc($classroom['capacity']); ?></span>
                            </div>
                            <div class="d-flex justify-content-between">
                                <div class="my-0">Количество окон: </div>
                                <div class="text-muted"><?= esc($classroom['windows']); ?></div>
                            </div>
                            <div class="d-flex justify-content-between">
                                <div class="my-0">Наличие интерактивной доски: </div>
                                <div class="text-muted"><?= esc($classroom['smartboards']); ?></div>
                            </div>
                            <div class="d-flex justify-content-between">
                                <div class="my-0">Наличие стационарного экрана: </div>
                                <div class="text-muted"><?= esc($classroom['stat_screens']); ?></div>
                            </div>
                            <div class="d-flex justify-content-between">
                                <div class="my-0">Наличие стационарного проектора: </div>
                                <div class="text-muted"><?= esc($classroom['stat_projectors']); ?></div>
                            </div>
                            <div class="d-flex justify-content-between">
                                <div class="my-0">Наличие меловой доски: </div>
                                <div class="text-muted"><?= esc($classroom['chalkboards']); ?></div>
                            </div>
                            <div class="d-flex justify-content-between">
                                <div class="my-0">Наличие компьютеров: </div>
                                <div class="text-muted"><?= esc($classroom['computers']); ?></div>
                            </div>
                            <div class="d-flex justify-content-between">
                                <div class="my-0">Наличие переносного проектора: </div>
                                <div class="text-muted"><?= esc($classroom['move_projectors']); ?></div>
                            </div>
                            <div class="d-flex justify-content-between">
                                <div class="my-0">Наличие раковины: </div>
                                <div class="text-muted"><?= esc($classroom['sinks']); ?></div>
                            </div>
                            <?php if (!empty($equipment)): ?>
                                <?php foreach ($equipment as $item): ?>
                                    <div class="d-flex justify-content-between">
                                        <div class ='my-0' ><?= esc($item['name']); ?>: </div>
                                        <div class ='text-muted' ><?= esc($item['value']); ?></div>
                                    </div>
                                <?php endforeach; ?>
                            <?php endif ?>
                            <div class="d-flex justify-content-between">
                                <div class="my-0">Примечание: </div>
                                <div class="text-muted"><?= esc($classroom['note']); ?></div>
                            </div>
                            <?php if ($ionAuth->loggedIn() && $ionAuth->isAdmin()): ?>
                                <a href="<?= base_url()?>/classroom/edit/<?= esc($classroom['id']); ?>" class="btn btn-primary">Редактировать</a>
                                <a href="<?= base_url()?>/classroom/delete/<?= esc($classroom['id']); ?>" class="btn btn-danger">Удалить</a>
                            <?php endif ?>
                        </div>
                    </div>
                </div>
            </div>
        <?php else : ?>
            <p>Аудитория не найдена.</p>
        <?php endif ?>
    </div>
<?= $this->endSection() ?>