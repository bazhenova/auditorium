<?= $this->extend('templates/layout') ?>
<?= $this->section('content') ?>
    <div class="container" style="max-width: 540px;">
        <?= form_open_multipart('classroom/store'); ?>
        <div class="form-group">
            <p class="text-center font-weight-bold">В полях, отмеченных * при отсутствии оборудования или парметров характеристик следует ввести 0.</p>
        </div>
        <div class="form-group">
            <label for="building">Корпус</label>
            <select class="form-select form-control <?= ($validation->hasError('building')) ? 'is-invalid' : ''; ?>" size="1" name="building" required>
                    <option selected disabled>Выберите корпус</option>
                    <option value="А">А</option>
                    <option value="Г">Г</option>
                    <option value="К">К</option>
                    <option value="С">С</option>
                    <option value="У">У</option>
            </select>
            <div class="invalid-feedback">
                <?= $validation->getError('building') ?>
            </div>
        </div>
        <div class="form-group">
            <label for="number">Номер</label>
            <input type="text" class="form-control <?= ($validation->hasError('number')) ? 'is-invalid' : ''; ?>" name="number"
                   value="<?= old('number'); ?>" placeholder="Примеры ввода номера: 401, 105/1, 105-1, 305/а, 305-а">
            <div class="invalid-feedback">
                <?= $validation->getError('number') ?>
            </div>
        </div>
        <div class="form-group">
            <label for="type">Назначение</label>
            <select class="form-select form-control <?= ($validation->hasError('type')) ? 'is-invalid' : ''; ?>" size="1" name="type" required>
                <option selected disabled>Выберите тип аудитории</option>
                <option value="лб">лб</option>
                <option value="км">км</option>
                <option value="нл">нл</option>
                <option value="пд">пд</option>
                <option value="пп">пп</option>
                <option value="пр">пр</option>
            </select>
            <div class="invalid-feedback">
                <?= $validation->getError('type') ?>
            </div>
        </div>
        <div class="form-group">
            <label for="department_id">Подразделение/кафедра</label>
            <select class="form-select form-control" size="1" name="department_id" required>
                <option selected disabled>Выберите подразделение/кафедру</option>
                <?php
                foreach ($department as $value) {
                    echo "<option value = {$value['id_d']} > {$value['name']} </option>";}
                ?>
            </select>
            <div class="invalid-feedback">
                <?= $validation->getError('department_id') ?>
            </div>
        </div>
        <div class="form-group">
            <label for="area">Площадь, кв. м</label>
            <input type="text" class="form-control <?= ($validation->hasError('area')) ? 'is-invalid' : ''; ?>" name="area"
                   value="<?= old('area'); ?>" placeholder="Пример ввода площади: 37, 19.1">
            <div class="invalid-feedback">
                <?= $validation->getError('area') ?>
            </div>
        </div>
        <div class="form-group">
            <label for="windows">Количество окон*</label>
            <input type="number" class="form-control <?= ($validation->hasError('windows')) ? 'is-invalid' : ''; ?>" name="windows"
                   value="<?= old('windows'); ?>">
            <div class="invalid-feedback">
                <?= $validation->getError('windows') ?>
            </div>
        </div>
        <div class="form-group">
            <label for="capacity">Количество рабочих мест*</label>
            <input type="number" class="form-control <?= ($validation->hasError('capacity')) ? 'is-invalid' : ''; ?>" name="capacity"
                   value="<?= old('capacity'); ?>">
            <div class="invalid-feedback">
                <?= $validation->getError('capacity') ?>
            </div>
        </div>
        <div class="form-group">
            <label for="smartboards">Наличие интерактивной доски*</label>
            <input type="number" class="form-control <?= ($validation->hasError('smartboards')) ? 'is-invalid' : ''; ?>" name="smartboards"
                   value="<?= old('smartboards'); ?>">
            <div class="invalid-feedback">
                <?= $validation->getError('smartboards') ?>
            </div>
        </div>
        <div class="form-group">
            <label for="stat_screens">Наличие стационарного экрана*</label>
            <input type="number" class="form-control <?= ($validation->hasError('stat_screens')) ? 'is-invalid' : ''; ?>" name="stat_screens"
                   value="<?= old('stat_screens'); ?>">
            <div class="invalid-feedback">
                <?= $validation->getError('stat_screens') ?>
            </div>
        </div>
        <div class="form-group">
            <label for="stat_projectors">Наличие стационарного проектора*</label>
            <input type="number" class="form-control <?= ($validation->hasError('stat_projectors')) ? 'is-invalid' : ''; ?>" name="stat_projectors"
                   value="<?= old('stat_projectors'); ?>">
            <div class="invalid-feedback">
                <?= $validation->getError('stat_projectors') ?>
            </div>
        </div>
        <div class="form-group">
            <label for="chalkboards">Наличие меловой доски*</label>
            <input type="number" class="form-control <?= ($validation->hasError('chalkboards')) ? 'is-invalid' : ''; ?>" name="chalkboards"
                   value="<?= old('chalkboards'); ?>">
            <div class="invalid-feedback">
                <?= $validation->getError('chalkboards') ?>
            </div>
        </div>
        <div class="form-group">
            <label for="computers">Наличие компьютеров*</label>
            <input type="number" class="form-control <?= ($validation->hasError('computers')) ? 'is-invalid' : ''; ?>" name="computers"
                   value="<?= old('computers'); ?>">
            <div class="invalid-feedback">
                <?= $validation->getError('computers') ?>
            </div>
        </div>
        <div class="form-group">
            <label for="move_projectors">Наличие переносного проектора*</label>
            <input type="number" class="form-control <?= ($validation->hasError('move_projectors')) ? 'is-invalid' : ''; ?>" name="move_projectors"
                   value="<?= old('move_projectors'); ?>">
            <div class="invalid-feedback">
                <?= $validation->getError('move_projectors') ?>
            </div>
        </div>
        <div class="form-group">
            <label for="sinks">Наличие раковины*</label>
            <input type="number" class="form-control <?= ($validation->hasError('sinks')) ? 'is-invalid' : ''; ?>" name="sinks"
                   value="<?= old('sinks'); ?>">
            <div class="invalid-feedback">
                <?= $validation->getError('sinks') ?>
            </div>
        </div>
        <?php if (!empty($equipment)): ?>
            <?php foreach ($equipment as $item): ?>
                <div class="form-group">
                    <label for="<?= esc($item['id']); ?>"><?= esc($item['name']); ?></label>
                    <input type="number" min="0" class="form-control" name="<?= esc($item['id']); ?>">
                </div>
            <?php endforeach; ?>
        <?php endif ?>
        <div class="form-group">
            <label for="note">Примечание</label>
            <textarea class="form-control <?= ($validation->hasError('note')) ? 'is-invalid' : ''; ?>" name="note"
                      value="<?= old('note'); ?>">
            </textarea>
            <div class="invalid-feedback">
                <?= $validation->getError('note') ?>
            </div>
        </div>
        <div class="form-group">
            <label for="picture">Изображение</label>
            <input type="file" class="form-control-file <?= ($validation->hasError('picture')) ? 'is-invalid' : ''; ?>" name="picture">
            <div class="invalid-feedback">
                <?= $validation->getError('picture') ?>
            </div>
        </div>
        <div class="form-group">
            <button type="submit" class="btn btn-primary" name="submit">Создать</button>
        </div>
        </form>
    </div>
<?= $this->endSection() ?>