<?= $this->extend('templates/layout') ?>
<?= $this->section('content') ?>
    <div class="container" style="max-width: 540px;">
        <h2>Удаление активов аудиторий</h2>
        <?= form_open('classroom/del'); ?>
        <div class="form-group">
            <label for="equipment_id">Выберите актив для удаления</label>
            <select class="form-select form-control" size="1" name="equipment_id" required>
                <?php
                foreach ($equipment as $value) {
                    echo "<option value = {$value['id']} > {$value['name']} </option>";}
                ?>
            </select>
        </div>
        <div class="form-group">
            <button type="submit" class="btn btn-danger" name="submit">Удалить</button>
        </div>
    </div>
<?= $this->endSection() ?>