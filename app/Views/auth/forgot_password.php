<?= $this->extend('templates/layout') ?>
<?= $this->section('content') ?>
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-6 col-12 ">
            <h1><?php echo lang('Auth.forgot_password_heading');?></h1>
            <p><?php echo sprintf(lang('Auth.forgot_password_subheading'), $identity_label);?></p>

            <div id="infoMessage"><?php echo $message;?></div>

            <?php echo form_open('auth/forgot_password');?>

            <p>
                <label for="identity"><?php echo (($type === 'email') ? sprintf(lang('Auth.forgot_password_email_label'), $identity_label) : sprintf(lang('Auth.forgot_password_identity_label'), $identity_label));?></label> <br />
                <?php echo form_input($identity,'','class="form-control" value="Mark" required');?>
            </p>

            <p><?php echo form_submit('submit', lang('Auth.forgot_password_submit_btn'), 'class="btn btn-primary"');?></p>

            <?php echo form_close();?>
        </div>
    </div>
</div>
<p class="text-center"><a href="register_user"><?php echo lang('Auth.register_user_link');?></a></p>
<?= $this->endSection() ?>
