-- phpMyAdmin SQL Dump
-- version 4.9.6
-- https://www.phpmyadmin.net/
--
-- Хост: localhost
-- Время создания: Май 19 2021 г., 00:08
-- Версия сервера: 8.0.21-0ubuntu0.20.04.4
-- Версия PHP: 7.4.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `auditorium`
--

-- --------------------------------------------------------

--
-- Структура таблицы `classroom`
--

CREATE TABLE `classroom` (
                             `id` int NOT NULL COMMENT 'id аудитории',
                             `building` varchar(1) NOT NULL COMMENT 'корпус',
                             `number` varchar(5) NOT NULL COMMENT 'номер',
                             `type` varchar(2) NOT NULL COMMENT 'назначение',
                             `department_id` int NOT NULL COMMENT 'подразделение/кафедра',
                             `area` decimal(5,1) NOT NULL COMMENT 'площадь',
                             `windows` int NOT NULL DEFAULT '0' COMMENT 'количество окон',
                             `capacity` int NOT NULL COMMENT 'вместимость',
                             `smartboards` int NOT NULL DEFAULT '0' COMMENT 'интерактивная доска',
                             `stat_screens` int NOT NULL DEFAULT '0' COMMENT 'стационарный экран',
                             `stat_projectors` int NOT NULL DEFAULT '0' COMMENT 'стационарный проектор',
                             `chalkboards` int NOT NULL DEFAULT '0' COMMENT 'меловая доска',
                             `computers` int NOT NULL DEFAULT '0' COMMENT 'компьютеры',
                             `move_projectors` int NOT NULL DEFAULT '0' COMMENT 'переносной проектор',
                             `sinks` int NOT NULL DEFAULT '0' COMMENT 'раковины',
                             `note` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT 'примечание'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `classroom`
--

INSERT INTO `classroom` (`id`, `building`, `number`, `type`, `department_id`, `area`, `windows`, `capacity`, `smartboards`, `stat_screens`, `stat_projectors`, `chalkboards`, `computers`, `move_projectors`, `sinks`, `note`) VALUES
(1, 'А', '320', 'км', 1, '37.0', 2, 13, 0, 0, 0, 1, 13, 1, 1, '            '),
(3, 'У', '105', 'лб', 1, '37.1', 0, 13, 0, 1, 0, 1, 9, 0, 1, NULL),
(5, 'У', '106', 'лб', 1, '37.1', 0, 13, 0, 1, 0, 1, 1, 0, 1, NULL),
(6, 'У', '401', 'лб', 1, '34.6', 2, 21, 0, 1, 0, 1, 1, 0, 1, NULL),
(15, 'А', '213', 'км', 2, '1.0', 1, 1, 1, 1, 1, 1, 1, 1, 1, '            '),
(16, 'Г', '12', 'лб', 2, '12.0', 12, 1, 1, 1, 0, 1, 1, 1, 0, '            '),
(17, 'У', '999', 'км', 1, '1.0', 1, 1, 1, 1, 1, 1, 1, 1, 1, '            '),
(21, 'К', '303', 'лб', 1, '19.9', 2, 2, 0, 0, 0, 0, 0, 0, 1, '            '),
(22, 'А', '305-а', 'лб', 2, '30.0', 1, 0, 0, 1, 1, 1, 1, 1, 1, '            ');

-- --------------------------------------------------------

--
-- Структура таблицы `department`
--

CREATE TABLE `department` (
                              `id_d` int NOT NULL COMMENT 'id подразделения/кафедры',
                              `name` varchar(255) NOT NULL COMMENT 'название',
                              `faculty_number` int DEFAULT NULL COMMENT 'номер кафедры'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `department`
--

INSERT INTO `department` (`id_d`, `name`, `faculty_number`) VALUES
(1, 'ПИ/Автоматики и компьютерных систем', 24),
(2, 'ИГОИС/Иностранных языков', 4),
(3, 'ИЭИУ/Менеджмента и бизнеса', 17);

-- --------------------------------------------------------

--
-- Структура таблицы `groups`
--

CREATE TABLE `groups` (
                          `id` mediumint UNSIGNED NOT NULL,
                          `name` varchar(20) NOT NULL,
                          `description` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `groups`
--

INSERT INTO `groups` (`id`, `name`, `description`) VALUES
(1, 'admin', 'Administrator'),
(2, 'members', 'General User');

-- --------------------------------------------------------

--
-- Структура таблицы `login_attempts`
--

CREATE TABLE `login_attempts` (
                                  `id` int UNSIGNED NOT NULL,
                                  `ip_address` varchar(45) NOT NULL,
                                  `login` varchar(100) NOT NULL,
                                  `time` int UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `users`
--

CREATE TABLE `users` (
                         `id` int UNSIGNED NOT NULL,
                         `ip_address` varchar(45) NOT NULL,
                         `username` varchar(100) DEFAULT NULL,
                         `password` varchar(255) NOT NULL,
                         `email` varchar(254) NOT NULL,
                         `activation_selector` varchar(255) DEFAULT NULL,
                         `activation_code` varchar(255) DEFAULT NULL,
                         `forgotten_password_selector` varchar(255) DEFAULT NULL,
                         `forgotten_password_code` varchar(255) DEFAULT NULL,
                         `forgotten_password_time` int UNSIGNED DEFAULT NULL,
                         `remember_selector` varchar(255) DEFAULT NULL,
                         `remember_code` varchar(255) DEFAULT NULL,
                         `created_on` int UNSIGNED NOT NULL,
                         `last_login` int UNSIGNED DEFAULT NULL,
                         `active` tinyint UNSIGNED DEFAULT NULL,
                         `first_name` varchar(50) DEFAULT NULL,
                         `last_name` varchar(50) DEFAULT NULL,
                         `company` varchar(100) DEFAULT NULL,
                         `phone` varchar(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `users`
--

INSERT INTO `users` (`id`, `ip_address`, `username`, `password`, `email`, `activation_selector`, `activation_code`, `forgotten_password_selector`, `forgotten_password_code`, `forgotten_password_time`, `remember_selector`, `remember_code`, `created_on`, `last_login`, `active`, `first_name`, `last_name`, `company`, `phone`) VALUES
(1, '127.0.0.1', 'administrator', '$2y$12$fQNQpMhdyaCOaZHStNxnkuTdWhOUNSQdgnED09rSYGfmxlOOSi3lm', 'admin@admin.com', NULL, '', NULL, NULL, NULL, NULL, NULL, 1268889823, 1621359566, 1, 'Admin', 'istrator', 'ADMIN', '0'),
(2, '::1', 'annbazhenova16@gmail.com', '$2y$10$jvobomtcajUz6A8oT0FIc.eCrKd.7LxuZO1HWpQzgozAu.NzZKkHe', 'annbazhenova16@gmail.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1621106793, 1621106800, 1, 'Анна', 'Баженова', NULL, '+79120867304');

-- --------------------------------------------------------

--
-- Структура таблицы `users_groups`
--

CREATE TABLE `users_groups` (
                                `id` int UNSIGNED NOT NULL,
                                `user_id` int UNSIGNED NOT NULL,
                                `group_id` mediumint UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `users_groups`
--

INSERT INTO `users_groups` (`id`, `user_id`, `group_id`) VALUES
(1, 1, 1),
(2, 1, 2),
(3, 2, 2);

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `classroom`
--
ALTER TABLE `classroom`
    ADD PRIMARY KEY (`id`),
  ADD KEY `department_id` (`department_id`);

--
-- Индексы таблицы `department`
--
ALTER TABLE `department`
    ADD PRIMARY KEY (`id_d`);

--
-- Индексы таблицы `groups`
--
ALTER TABLE `groups`
    ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `login_attempts`
--
ALTER TABLE `login_attempts`
    ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `users`
--
ALTER TABLE `users`
    ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `uc_email` (`email`),
  ADD UNIQUE KEY `uc_activation_selector` (`activation_selector`),
  ADD UNIQUE KEY `uc_forgotten_password_selector` (`forgotten_password_selector`),
  ADD UNIQUE KEY `uc_remember_selector` (`remember_selector`);

--
-- Индексы таблицы `users_groups`
--
ALTER TABLE `users_groups`
    ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `uc_users_groups` (`user_id`,`group_id`),
  ADD KEY `fk_users_groups_users1_idx` (`user_id`),
  ADD KEY `fk_users_groups_groups1_idx` (`group_id`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `classroom`
--
ALTER TABLE `classroom`
    MODIFY `id` int NOT NULL AUTO_INCREMENT COMMENT 'id аудитории', AUTO_INCREMENT=24;

--
-- AUTO_INCREMENT для таблицы `department`
--
ALTER TABLE `department`
    MODIFY `id_d` int NOT NULL AUTO_INCREMENT COMMENT 'id подразделения/кафедры', AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT для таблицы `groups`
--
ALTER TABLE `groups`
    MODIFY `id` mediumint UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT для таблицы `login_attempts`
--
ALTER TABLE `login_attempts`
    MODIFY `id` int UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `users`
--
ALTER TABLE `users`
    MODIFY `id` int UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT для таблицы `users_groups`
--
ALTER TABLE `users_groups`
    MODIFY `id` int UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- Ограничения внешнего ключа сохраненных таблиц
--

--
-- Ограничения внешнего ключа таблицы `classroom`
--
ALTER TABLE `classroom`
    ADD CONSTRAINT `classroom_ibfk_1` FOREIGN KEY (`department_id`) REFERENCES `department` (`id_d`) ON DELETE RESTRICT ON UPDATE RESTRICT;

--
-- Ограничения внешнего ключа таблицы `users_groups`
--
ALTER TABLE `users_groups`
    ADD CONSTRAINT `fk_users_groups_groups1` FOREIGN KEY (`group_id`) REFERENCES `groups` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `fk_users_groups_users1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
